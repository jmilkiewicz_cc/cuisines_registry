package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static de.quandoo.recruitment.registry.MotherUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class InMemoryCuisinesRegistryShallReturnCuisineCustomersTest {

    private InMemoryCuisinesRegistry sut = new InMemoryCuisinesRegistry();

    @Test
    public void shallReturnCustomerForFrenchCuisine() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer2, germanCuisine);
        sut.register(customer3, italianCuisine);

        List<Customer> frenchCuisineLovers = sut.cuisineCustomers(new Cuisine(frenchCuisine.getName()));

        assertThat(frenchCuisineLovers, containsInAnyOrder(new Customer(customer1.getUuid())));
    }

    @Test
    public void shallReturnCustomerForGermanCuisine() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer2, germanCuisine);

        assertThat(sut.cuisineCustomers(germanCuisine), containsInAnyOrder(customer2));
    }

    @Test
    public void shallReturnCustomerOnceEvenWhenRegisteredMultipleTimesForGiveCuisine() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer2, germanCuisine);
        sut.register(customer1, frenchCuisine);

        List<Customer> frenchCuisineLovers = sut.cuisineCustomers(frenchCuisine);

        assertThat(frenchCuisineLovers, containsInAnyOrder(customer1));
    }

    @Test
    public void shallReturnMultipleRegisteredCustomersForCuisine() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer3, germanCuisine);
        sut.register(customer2, frenchCuisine);

        List<Customer> frenchCuisineLovers = sut.cuisineCustomers(frenchCuisine);

        assertThat(frenchCuisineLovers, containsInAnyOrder(customer1, customer2));
    }


    @Test(expected = RuntimeException.class)
    public void shallThrowRuntimeExceptionOnRegisterWhenCuisineIsNull() {
        sut.register(customer1, null);
    }

    @Test(expected = RuntimeException.class)
    public void shallThrowRuntimeExceptionOnRegisterWhenCustomerIsNull() {
        sut.register(null, frenchCuisine);
    }

    @Test(expected = RuntimeException.class)
    public void shallThrowRuntimeExceptionWhenInputParameterIsNull() {
        sut.cuisineCustomers(null);
    }

}