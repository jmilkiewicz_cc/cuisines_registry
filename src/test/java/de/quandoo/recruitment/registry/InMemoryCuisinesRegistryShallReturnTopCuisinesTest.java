package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static de.quandoo.recruitment.registry.MotherUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class InMemoryCuisinesRegistryShallReturnTopCuisinesTest {
    private InMemoryCuisinesRegistry sut = new InMemoryCuisinesRegistry();

    @Test
    public void shallReturnCuisinesOrderedByNumberOfCustomers() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer1, germanCuisine);
        sut.register(customer3, frenchCuisine);

        List<Cuisine> cuisines = sut.topCuisines(Integer.MAX_VALUE);

        assertThat(cuisines, contains(frenchCuisine, germanCuisine));
    }


    @Test
    public void shallReturnOnlyUpToNCuisines() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer2, germanCuisine);
        sut.register(customer2, frenchCuisine);

        List<Cuisine> cuisines = sut.topCuisines(1);

        assertThat(cuisines, contains(frenchCuisine));
    }


    @Test
    public void shallReturnRandomTopCuisineInCaseOfDraw() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer1, germanCuisine);
        sut.register(customer2, germanCuisine);
        sut.register(customer3, frenchCuisine);
        sut.register(customer3, italianCuisine);

        List<Cuisine> cuisines = sut.topCuisines(1);

        assertThat(cuisines, contains(Matchers.isOneOf(frenchCuisine, germanCuisine)));
    }

    @Test(expected = RuntimeException.class)
    public void shallThrowRuntimeExceptionWhenParameterIsNegative() {
        sut.topCuisines(-1);
    }

}