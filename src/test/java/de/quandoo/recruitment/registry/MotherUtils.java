package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class MotherUtils {
    public static final Customer customer1 = new Customer("1");
    public static final Customer customer2 = new Customer("2");
    public static final Customer customer3 = new Customer("3");
    public static final Cuisine frenchCuisine = new Cuisine("french");
    public static final Cuisine germanCuisine = new Cuisine("german");
    public static final Cuisine italianCuisine = new Cuisine("italian");
}
