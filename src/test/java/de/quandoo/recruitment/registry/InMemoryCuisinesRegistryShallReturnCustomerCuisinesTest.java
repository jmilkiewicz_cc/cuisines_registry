package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import org.junit.Test;

import java.util.List;

import static de.quandoo.recruitment.registry.MotherUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;

public class InMemoryCuisinesRegistryShallReturnCustomerCuisinesTest {
    private InMemoryCuisinesRegistry sut = new InMemoryCuisinesRegistry();

    @Test
    public void shallReturnAllCuisinesForCustomer() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer3, italianCuisine);
        sut.register(customer1, germanCuisine);


        List<Cuisine> cuisines = sut.customerCuisines(customer1);

        assertThat(cuisines, containsInAnyOrder(frenchCuisine, germanCuisine));
    }

    @Test
    public void shallReturnEmptyListWhenCustomerHasNotBeRegistered() {
        sut.register(customer1, frenchCuisine);
        sut.register(customer3, italianCuisine);

        List<Cuisine> cuisines = sut.customerCuisines(customer2);

        assertThat(cuisines, emptyIterable());
    }

    @Test(expected = RuntimeException.class)
    public void shallThrowRuntimeExceptionWhenCustomerParamIsNull() {
        sut.customerCuisines(null);
    }

}