package de.quandoo.recruitment.registry;

import com.google.common.base.Preconditions;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
    private final ConcurrentMap<Cuisine, Set<Customer>> cuisineToCustomers = new ConcurrentHashMap();
    private final Comparator<Map.Entry<Cuisine, Set<Customer>>> byNumberOfCustomers =
            Comparator.<Map.Entry<Cuisine, Set<Customer>>>comparingInt(e -> e.getValue().size()).reversed();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        Preconditions.checkNotNull(cuisine, "cuisine can not be null");
        Preconditions.checkNotNull(customer, "customer can not be null");
        cuisineToCustomers.merge(cuisine, wrapWithConcurrentSet(customer), (old, newCustomer) -> {
            old.addAll(newCustomer);
            return old;
        });
    }

    private Set<Customer> wrapWithConcurrentSet(Customer customer) {
        ConcurrentHashMap.KeySetView<Customer, Boolean> result = ConcurrentHashMap.newKeySet();
        result.add(customer);
        return result;
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        Set<Customer> customers = cuisineToCustomers.getOrDefault(Preconditions.checkNotNull(cuisine), Collections.emptySet());
        return new LinkedList<>(customers);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        Preconditions.checkNotNull(customer, "customer can not be null");
        return cuisineToCustomers.entrySet().stream()
                .filter(entry -> entry.getValue().contains(customer))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        //here probably makes sense to set n to default value (like 1) if n is negative
        Preconditions.checkArgument(n >= 0, "must not be negative");
        return cuisineToCustomers.entrySet().stream()
                .sorted(byNumberOfCustomers)
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
