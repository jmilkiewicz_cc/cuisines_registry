# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## My Solution (more than 100 words):
I decided to time-box my work: as the software is pretty trivial but making it bullet proof is really hard.
I made a couple of assumptions:
- InMemoryCuisinesRegistry methods return list, that implies a structure with some order. For the sake of this coding challenge 
i decided to alleviate it and treat these lists as sets, so there is no discussion if these lists shall be somehow sorted (insert-order, last used order)
- I introduced a naive parameter validation (as being suggested by tests provided) via Preconditions but i believe the real validation shall be performed in other layer
- I tried to make InMemoryCuisinesRegistry thread-safe albeit this requirement was not explicitly specified in the challenge description. 
All my efforts are mostly based on my knowledge/experience and javadoc. To make it more robust i would try to build a dedicated test suite to run multiple 
threads running against InMemoryCuisinesRegistry...

Regarding scaling... it is a hard topic as it is not easy to scale this component as a whole (at least i do not know how to do it nicely).
The component has mostly 4 responsibilities:
- registering customer for cuisine
- getting list of customers for a given cuisine
- getting list of cuisines for a given customer
- getting most popular cuisines

Particular data structures allows one to achieve high performance and strong consistency for some of these operations 
at the same time making other operations slower and/or more complex to implement. In my solution i decided to keep 
registration and getting list of customers for cuisine the most performant (and consistent). The other 2 operations (list of cuisines for a given customer/most popular cuisines)
require more data crunching so looks like being less optimised than the other 2. It was simply my choice.
These 2 "less optimised" operations can be implemented with additional structures (like map from customer->cuisine , map from cuisine -> number of customers registered)
but they would work more as caches, with a standard cache consistency problems. It can happen that registration would require updating not a single data structure (like in my solution)         
but a couple of them. As a registration thread can be suspended any time it can happen that it would update only some part of data structures 
before being suspended. At the same time other "readers" can read from these data structures and provide inconsistent results...
It can be solved by using explicit synchronisation but i am not sure if it is not a killer for performance...

We can also decided to scale it horizontally: keep data on multiple nodes sharded (and probably replicated). 
In case of billions of customers it would make sense to simply shard it by customer (maybe using consistent hashing to find where customer data is) - I imagine number of customers 
will definitely grow faster than number of cuisines. It would require to keep cuisines as a reference data (sorry i am not buying that there are million kind of cuisines) in each shard.

If business condition allows it i would rather split the component into 2 separated components: 
- user-facing for registration and keeping cuisines for customers
- analytics-facing for keeping list of customers for a given cuisine and top popular cuisines

Customers would use the first one, and on successful registration "an event" to analytics-facing application would be fired 
to update its state. This way we can scale both of them independently but paying a price of eventual consistency....
